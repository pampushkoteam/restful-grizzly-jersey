/*
 */

package pampushko.tests.grizzlyRest;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;


// TODO: Auto-generated Javadoc

/**
 * Этот test case класс демонстрирует использование встроенного API.
 *
 * @author alexander.pampushko
 */
@Ignore
public class ExtreamResourceTest
{
	/**
	 Константа содержащая BASE_URI : Базовый адрес встроенного сервера.
	 */
	public static final String BASE_URI = "http://localhost:8084/";
	
	/**
	 Константа содержащая BASE_API_URI : Базовый адрес по которому Jersey будет обрабатывать API-запросы
	 */
	public static final String BASE_API_URI = "http://localhost:8084/api";
	
	
	/**
	 * Константа START_URI, содержит URL REST метода start.
	 */
	public static final String START_URI = "http://localhost:8084/api/start";
	
	/**
	 * Константа STOP_URI, содержит URL метода stop
	 */
	public static final String STOP_URI = "http://localhost:8084/api/stop";
	
	/**
	 * Константа RELOAD_URI, содержит URL метода reload
	 */
	public static final String RELOAD_URI = "http://localhost:8084/api/reload";
	
	/**
	 * http сервер
	 */
	private HttpServer httpServer;
	
	/**
	 * http клиент
	 */
	private CloseableHttpClient httpClient;
	
	/**
	 * <p>
	 * в секции теста "setup", мы должны стартовать наш встроенный
	 * сервер который начнет слушать HTTP запросы на определенные URL.
	 * </p>
	 * Это то что должен делать обычно код вашего приложения.
	 * @throws Exception метод выбрасывает исключение
	 */
	@Before
	public void setUp() throws Exception
	{
		//стартуем сервер
		this.httpServer = StartGizzly.startServer();
		
		//создаем клиента
		this.httpClient = HttpClients.createDefault();
	}
	
	/**
	 * <p>
	 *     В части теста "tear-down", мы должны остановить наш встроенный сервер.
	 * </p>
	 * <p>
	 *     Это то что должно произойти при нашем выходе из приложения
	 * </p>
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception
	{
		this.httpServer.shutdownNow();
	}
	
	/**
	 * Тест вызывает root API метод:
	 * http://[server]:[port]/api.
	 */
	@Test
	public void testGetIt()
	{
		testHttp(BASE_API_URI);
		//assertEquals("Got it!", responseMsg);
	}
	
	/**
	 * Test the call to the start API method:
	 * "http://[server]:[port]/api/start."
	 */
	@Test
	public void testStart()
	{
		testHttp(START_URI);
	}
	
	/**
	 * Test the call to the stop API method:
	 * "http://[server]:[port]/api/stop."
	 */
	@Test
	public void testStop()
	{
		testHttp(STOP_URI);
	}
	
	/**
	 * Test the call to the stop API method:
	 * "http://[server]:[port]/api/stop."
	 */
	@Test
	public void testReload()
	{
		testHttp(RELOAD_URI);
	}
	
	/**
	 * Test http вызывает определенный запрос и проверяет статус-код (200 = OK),
	 * который должен быть возвращен в ответе
	 *
	 * @param uri адрес (uri) куда вы хотите послать тестовый запрос
	 */
	private void testHttp(String uri)
	{
		try
		{
			HttpGet request = new HttpGet(uri);
			CloseableHttpResponse response = this.httpClient.execute(request);
			
			String body = EntityUtils.toString(response.getEntity());
			System.out.println(body);
			
			int rc = response.getStatusLine().getStatusCode();
			assertEquals(rc, 200);
			
			response.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			fail(e.getMessage());
		}
	}
}
