create table bookmark (
  id          uuid   not null,
  created_by  varchar(255),
  updated_by  varchar(255),
  created_at  timestamptz,
  updated_at  timestamptz,
  url         varchar(255),
  name        varchar(255),
  description varchar(255),
  version     bigint not null,
  constraint pk_bookmark primary key (id)
);

create table bookmark_tag (
  bookmark_id uuid not null,
  tag_id      uuid not null,
  constraint pk_bookmark_tag primary key (bookmark_id, tag_id)
);

create table tag (
  id   uuid not null,
  name varchar(255),
  constraint pk_tag primary key (id)
);

create index ix_bookmark_tag_bookmark
  on bookmark_tag (bookmark_id);
alter table bookmark_tag
  add constraint fk_bookmark_tag_bookmark foreign key (bookmark_id) references bookmark (id) on delete restrict on update restrict;

create index ix_bookmark_tag_tag
  on bookmark_tag (tag_id);
alter table bookmark_tag
  add constraint fk_bookmark_tag_tag foreign key (tag_id) references tag (id) on delete restrict on update restrict;

