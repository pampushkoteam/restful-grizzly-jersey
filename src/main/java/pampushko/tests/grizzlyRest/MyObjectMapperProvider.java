package pampushko.tests.grizzlyRest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

/**
 * Провайдер JSON который так и не понадобился,
 * <br>
 * но возможно пригодится в дальнешем.
 */
//@Provider
//public class MyObjectMapperProvider implements ContextResolver<ObjectMapper> {
public class MyObjectMapperProvider
{
	
	final ObjectMapper defaultObjectMapper;
	
	public MyObjectMapperProvider()
	{
		defaultObjectMapper = createDefaultMapper();
	}
	
	//@Override
	public ObjectMapper getContext(final Class<?> type)
	{
		
		return defaultObjectMapper;
	}
	
	private static ObjectMapper createDefaultMapper()
	{
		final ObjectMapper result = new ObjectMapper();
		result.enable(SerializationFeature.INDENT_OUTPUT);
		
		return result;
	}
	
}