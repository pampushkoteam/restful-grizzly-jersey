/**
 */
package pampushko.tests.grizzlyRest;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;

/**
 * Главный встроенный HTTP сервер, который хостит у себя Jersey.
 * <br>
 * Не забудьте остановить сервер перед тем как закрыть завершить процесс выполнения программы
 * @author Alexander.Pampushko @link "http://java-java.atlassian.net"
 */
public class StartGizzly
{
	//Базовый URL, и порт, который будет слушать HTTP server Grizzly
	public static final String BASE_URI = "http://localhost:8084/";
	
	/**
	 * Стартует Grizzly HTTP сервер, который будет предоставлять JAX-RS ресурсы
	 * <br>
	 * определённые в Jersey
	 * @return Grizzly HTTP сервер
	 */
	public static HttpServer startServer()
	{
		//создаем файл настройки Jersey
		final ResourceConfig jerseyConfiguration = new JerseyConfiguration();
		
		//создаем и стартуем новый экземпляр grizzly http-сервера
		//который выставляет Jersey приложение на BASE_URI
		return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), jerseyConfiguration);
	}
	
	/**
	 * Главная точка входа // только для теста
	 *
	 * @param args description
	 * @throws IOException description
	 */
	public static void main(String[] args) throws IOException
	{
		//запускаем http-сервер
		final HttpServer server = startServer();
		
		//выводим в консоль сообщение о старте сервера на таком-то BASE_URI
		System.out.println(String.format("Grizzly hosting Jersey app listening at %s\n"
				+ "Hit enter to stop it...", BASE_URI));
		
		//ждем нажатия пользователем клавиши для остановки сервера
		System.in.read();
		
		//останавливаем сервер
		server.shutdownNow();
	}
}
