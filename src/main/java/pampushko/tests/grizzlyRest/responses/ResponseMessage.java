/*
 * eXelate benchmarks suite to test the usage and performance of Java frameworks
 * for the next generation serving architecture.
 * 
 * @author {@link "mailto:mottyc@exelate.com"}
 * 
 * Copyright (C) 2012 eXelate, ALL RIGHTS RESERVED
 */

package pampushko.tests.grizzlyRest.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Класс ответов на HTTP-запросы
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResponseMessage
{
	/***
	 * код возврата
	 */
	private int code;
	
	/**
	 * сообщение
	 */
	private String message;
	
	/**
	 * прошедшее время
	 */
	private long elapsedTime;
}
