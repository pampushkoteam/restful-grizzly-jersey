package pampushko.tests.grizzlyRest;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.ApplicationPath;
import java.lang.invoke.MethodHandles;

/**
 *
 */
@ApplicationPath("/api")
public class JerseyConfiguration extends ResourceConfig
{
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	public JerseyConfiguration()
	{
		property(ServerProperties.TRACING, "ALL"); //enable tracing support
		property(ServerProperties.TRACING_THRESHOLD, "VERBOSE");
		property(ServerProperties.MOXY_JSON_FEATURE_DISABLE, "true");
		property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, "true");
		
		log.info("регистрируем REST endpoints");
		register(new AppHK2Binder());
		
		//этими двумя строками мы используем напрямую JacksonJsonProvider, а не тот который идет вместе с Jersey:
		//maven-зависимость jersey-media-json-jackson нами больше не используется
		//как и класс JacksonFeature.class
		//JacksonFeature.class
		packages("com.fasterxml.jackson.jaxrs.base");
		register(JacksonJsonProvider.class);
		
		//эти классы мы не регистрируем, так как ниже обходим рекурсивно весь пакет
		//register(MyObjectMapperProvider.class);
		//register(BookmarkResource.class);
		//register(CustomLoggingFilter.class);
		
		packages(true, "pampushko.tests.grizzlyRest"); //recursive = true
	}
	
}

