package pampushko.tests.grizzlyRest.services;

import io.ebean.EbeanServer;
import io.ebean.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pampushko.tests.grizzlyRest.entity.Bookmark;
import pampushko.tests.grizzlyRest.entity.Tag;

import javax.inject.Inject;
import java.lang.invoke.MethodHandles;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by algernon on 14/08/2017.
 */
public class BookmarkService implements BookmarkServiceInterface
{
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	private static AtomicInteger counter = new AtomicInteger();
	
	@Inject
	private EbeanServer server;
	
	public Bookmark getBookmark(final String id)
	{
		Bookmark bookmark = server.find(Bookmark.class, id);
		if (bookmark != null)
		{
			return bookmark;
		}
		else
		{
			return null;
		}
	}
	
	/**
	 * Добавляет новую закладку
	 * @param bookmark
	 * @return
	 */
	public Bookmark addBookmark(Bookmark bookmark)
	{
		HashSet<Tag> resultTagList = new HashSet<>();
		bookmark.getTagList().forEach(tag ->
		{
			Query<Tag> tagQuery = server.find(Tag.class).where().eq("name", tag.getName()).query();
			List<Tag> list = tagQuery.findList();
			if (list.size() > 0)
			{
				resultTagList.add(list.get(0));
			}
			else
			{
				server.save(tag);
				resultTagList.add(tag);
			}
		});
		bookmark.setTagList(resultTagList);
		server.insert(bookmark);
		return bookmark;
	}
	
	/**
	 * Редактирует закладку
	 * @return
	 */
	public Bookmark editBookmark(String id, Bookmark newBookmark)
	{
		Bookmark bookmarkForUpdate = server.find(Bookmark.class, id);
		if (bookmarkForUpdate != null)
		{
			bookmarkForUpdate.setName(newBookmark.getName());
			bookmarkForUpdate.setDescription(newBookmark.getDescription());
			bookmarkForUpdate.setUrl(newBookmark.getUrl());
			//bookmarkForUpdate.setTagList(newBookmark.getTagList());
			server.save(bookmarkForUpdate);
			return bookmarkForUpdate;
		}
		return null;
	}
	
	/**
	 * Удаляет закладку по идентификатору
	 * @param id
	 * @return
	 */
	public Boolean deleteBookmark(String id)
	{
		Bookmark bookmark = server.find(Bookmark.class, id);
		if (bookmark != null)
		{
			return bookmark.delete();
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Находит закладку по идентификатору
	 * @param id
	 * @return
	 */
	public Bookmark findBookmark(String id)
	{
		Bookmark bookmark = server.find(Bookmark.class, id);
		return bookmark;
	}
	
	/**
	 * Возвращает список всех закладок
	 * @return
	 */
	public List<Bookmark> getAllBookmarksList()
	{
		List<Bookmark> list = server.find(Bookmark.class).findList();
		return list;
	}
}
