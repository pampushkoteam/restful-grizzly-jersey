package pampushko.tests.grizzlyRest.services;

import pampushko.tests.grizzlyRest.entity.Bookmark;

import java.util.List;

/**
 * Created by algernon on 14/08/2017.
 */
public interface BookmarkServiceInterface
{
	/**
	 * Получает закладку по идентификатору
	 *
	 * @param id
	 *
	 * @return
	 */
	Bookmark getBookmark(final String id);
	
	/**
	 * Добавляет новую закладку
	 *
	 * @param bookmark
	 *
	 * @return
	 */
	Bookmark addBookmark(Bookmark bookmark);
	
	/**
	 * Редактирует закладку
	 *
	 * @return
	 */
	Bookmark editBookmark(String id, Bookmark newBookmark);
	
	/**
	 * Удаляет закладку по идентификатору
	 *
	 * @param id
	 *
	 * @return
	 */
	Boolean deleteBookmark(String id);
	
	/**
	 * Находит закладку по идентификатору
	 *
	 * @param id
	 *
	 * @return
	 */
	Bookmark findBookmark(String id);
	
	/**
	 * Возвращает список всех закладок
	 *
	 * @return
	 */
	List<Bookmark> getAllBookmarksList();
}
