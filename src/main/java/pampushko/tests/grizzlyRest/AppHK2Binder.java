package pampushko.tests.grizzlyRest;

import pampushko.tests.grizzlyRest.services.BookmarkService;
import io.ebean.EbeanServer;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.lang.invoke.MethodHandles;

/**
 * Настраиваем HK2
 */
public class AppHK2Binder extends AbstractBinder
{
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	@Override
	protected void configure()
	{
		bindAsContract(BookmarkService.class);
		
		//https://stackoverflow.com/questions/49566860/hk2-factory-implementation-singleton
		//биндим фабрику как синглтон и ебин-сервер, который производит фабрика как синглтон
		bindFactory(SimpleEbeanServerFactory.class, Singleton.class).to(EbeanServer.class).in(Singleton.class);
	}
}
