package pampushko.tests.grizzlyRest.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import java.sql.Timestamp;
import java.util.UUID;

/**
 *
 */
@MappedSuperclass
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Slf4j
public class BaseModel extends Model
{
	@Id
	@GeneratedValue
	private UUID id;
	
	@Version
	private Long version;
	//@WhoCreated
	private String createdBy;
	//@WhoModified
	private String updatedBy;
	
	@CreatedTimestamp
	private Timestamp createdAt;
	@UpdatedTimestamp
	private Timestamp updatedAt;
	
	@Override
	public String toString()
	{
		String result = "";
		ObjectMapper objectMapper = new ObjectMapper();
		try
		{
			result = objectMapper.writeValueAsString(this);
		}
		catch (JsonProcessingException ex)
		{
			log.error("ошибка вывода объекта в String", ex);
		}
		return result;
	}
	
}
