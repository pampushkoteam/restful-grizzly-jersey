package pampushko.tests.grizzlyRest.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 *
 */
@Entity
public class SystemUser extends BaseModel
{
	@Column(length = 100)
	private String username;
	
	@Column(length = 100)
	private String password;
}
