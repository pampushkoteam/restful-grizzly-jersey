package pampushko.tests.grizzlyRest.entity;

import io.ebean.Model;
import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.UpdatedTimestamp;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

/**
 *
 */
@Entity
@Table(name = "tag")
@Getter
@Setter
@ToString(callSuper = true, doNotUseGetters = true)
//todo уточнить что там переопределяет lombok такого, что мешает нормальной сериализации объекта в JSON
//todo @ToString doNotUserGetter = false > error
//todo @EqualsAndHashCode(callSuper = true) > error
public class Tag extends Model
{
	private static final long serialVersionUID = 123L;
	
	@Id
	@GeneratedValue
	private UUID id;
	
	@Version
	private Long version;
	//@WhoCreated
	private String createdBy;
	//@WhoModified
	private String updatedBy;
	
	@CreatedTimestamp
	private Timestamp createdAt;
	@UpdatedTimestamp
	private Timestamp updatedAt;
	
	@Column(unique = true)
	private String name;
}
