package pampushko.tests.grizzlyRest.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */
@Entity
@Table(name = "bookmark")
@Getter
@Setter
@ToString(callSuper = true, doNotUseGetters = true)
//todo уточнить что там переопределяет lombok такого, что мешает нормальной сериализации объекта в JSON
//todo @ToString doNotUserGetter = false > error
//todo @EqualsAndHashCode(callSuper = true) > error
public class Bookmark extends BaseModel
{
	private static final long serialVersionUID = 124443L;
	private String url;
	private String name;
	private String description;
	
	@ManyToMany
	private Set<Tag> tagList = new HashSet<Tag>();
	
	public Bookmark()
	{
	
	}
}
