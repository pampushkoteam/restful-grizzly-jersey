package pampushko.tests.grizzlyRest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pampushko.tests.grizzlyRest.constants.RegexpPatterns;
import pampushko.tests.grizzlyRest.entity.Bookmark;
import pampushko.tests.grizzlyRest.responses.ErrorMessage;
import pampushko.tests.grizzlyRest.responses.StatusMessage;
import pampushko.tests.grizzlyRest.services.BookmarkService;

import javax.inject.Inject;
import javax.validation.constraints.Pattern;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.invoke.MethodHandles;
import java.util.List;

/**
 *
 */
@Path("api/bookmark")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class BookmarkResource
{
	private final static Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	@Inject
	public BookmarkService bookmarkService;
	
	@GET
	@Path("/{id}")
	public Response getBookmark(@PathParam("id") @Pattern(regexp = RegexpPatterns.UUID_PATTERN) final String id)
	{
		log.info("пытаемся получить закладку");
		Bookmark bookmark = bookmarkService.getBookmark(id);
		if (bookmark != null)
		{
			return Response.ok(bookmark, MediaType.APPLICATION_JSON).build();
		}
		else
		{
			return Response.serverError().entity(new ErrorMessage("не удалось найти закладку по идентификатору")).type
					(MediaType.APPLICATION_JSON)
					.build();
		}
	}
	
	@GET
	public Response getBookmarkList()
	{
		List<Bookmark> allBookmarksList = this.bookmarkService.getAllBookmarksList();
		log.info("получили список закладок", allBookmarksList);
		Response response = Response.ok(allBookmarksList, MediaType.APPLICATION_JSON).build();
		log.info("сформировали ответ", response);
		return response;
	}
	
	@POST
	public Response createBookmark(Bookmark bookmark)
	{
		Bookmark savedBookmark = this.bookmarkService.addBookmark(bookmark);
		log.info("создали новую закладку");
		return Response.ok(bookmark, MediaType.APPLICATION_JSON_TYPE).build();
	}
	
	@PUT
	@Path("/{id}")
	public Response updateBookmark(@PathParam("id") @Pattern(regexp = RegexpPatterns.UUID_PATTERN) final String id,
	                               Bookmark bookmark)
	{
		Bookmark updatedBookmark = this.bookmarkService.editBookmark(id, bookmark);
		if (updatedBookmark != null)
		{
			log.info("обновили запись о закладке");
			return Response.ok(updatedBookmark, MediaType.APPLICATION_JSON).build();
		}
		else
		{
			log.info("не удалось обновить запись о закладке");
			
			//.put("message", "не удалось обновить запись о закладке");
			return Response.serverError().entity(new ErrorMessage("не удалось обновить запись о закладке")).type
					(MediaType.APPLICATION_JSON)
					.build();
		}
	}
	
	@DELETE
	@Path("/{id}")
	public Response deleteBookmark(@PathParam("id") @Pattern(regexp = RegexpPatterns.UUID_PATTERN) final String id)
	{
		log.info("удалили закладку");
		Boolean deleteBookmarkStatus = this.bookmarkService.deleteBookmark(id);
		return Response.ok(new StatusMessage(deleteBookmarkStatus), MediaType.APPLICATION_JSON).build();
	}
}
