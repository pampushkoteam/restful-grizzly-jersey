package pampushko.tests.grizzlyRest;

import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.message.internal.ReaderWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;

/**
 *
 */
@Provider
public class CustomLoggingFilter extends LoggingFeature implements ContainerRequestFilter, ContainerResponseFilter
{
	private final static Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	private final static String DELIM_STRING = System.lineSeparator() + "\u001B[31m" + "░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░" + "\u001B[0m" + System.lineSeparator();
	
	/**
	 * Здесь мы делаем что-то со входящим запросом
	 * @param requestContext
	 * @throws IOException
	 */
	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException
	{
		StringBuilder sb = new StringBuilder();
		
		// Append username is available
		String user = null;
//		try {
//			user = new LoginData(requestContext.getHeaders().get("Authorization").get(0)).getUsername();
//		} catch (Exception e) {
//		}
		sb.append(DELIM_STRING);
		sb.append("ВХОДЯЩИЙ ЗАПРОС:");
		sb.append(DELIM_STRING);
		sb.append("User: ").append(user);
		sb.append(DELIM_STRING);
		sb.append(" - Path: ").append(requestContext.getUriInfo().getPath());
		sb.append(DELIM_STRING);
		sb.append(" - Header: ").append(requestContext.getHeaders());
		
		// Append entity shortened to 10000 chars
		String entity = getEntityBody(requestContext);
		entity = entity.replace("\n", "").replace("\r", "");
		if (entity.length() > 10001)
		{
			entity.substring(0, 9999);
		}
		sb.append(DELIM_STRING);
		sb.append(" - Entity: ").append(entity);
		sb.append(DELIM_STRING);
		
		logger.info("REST " + requestContext.getMethod() + " Request : " + sb.toString());
	}
	
	/**
	 * В этом методе мы получаем из входящего запроса тело
	 * <p>
	 * и возвращаем его в виде строки.
	 * @param requestContext
	 * @return
	 */
	private String getEntityBody(ContainerRequestContext requestContext)
	{
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		InputStream in = requestContext.getEntityStream();
		final StringBuilder b = new StringBuilder();
		try
		{
			ReaderWriter.writeTo(in, out);
			byte[] requestEntity = out.toByteArray();
			if (requestEntity.length == 0)
			{
				b.append("");
			} else
			{
				b.append(new String(requestEntity));
			}
			requestContext.setEntityStream(new ByteArrayInputStream(requestEntity));
		} catch (Exception ex)
		{
			return "";
		}
		
		return b.toString();
	}
	
	/**
	 * Здесь мы что-то делаем с ответом на запрос.
	 * <p>
	 * Ответ на запрос мы отправляем клиенту.
	 * @param requestContext контекст запроса
	 * @param responseContext контекст ответа на запрос
	 * @throws IOException
	 */
	@Override
	public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
			throws IOException
	{
		addHeader(responseContext);
		
		StringBuilder sb = new StringBuilder();
		sb.append(DELIM_STRING);
		sb.append("ОТВЕТ НА ЗАПРОС:");
		sb.append(DELIM_STRING);
		sb.append("Header: ").append(responseContext.getHeaders());
		sb.append(DELIM_STRING);
		sb.append(" - Entity: ");
		if (responseContext != null && responseContext.getEntity() != null)
		{
			sb.append(responseContext.getEntity().toString().replace("\n", "").replace("\r", ""));
		} else
		{
			sb.append("null");
		}
		
		sb.append(DELIM_STRING);
		if (responseContext.getStatusInfo().getFamily() == Response.Status.Family.SUCCESSFUL
				|| responseContext.getStatusInfo().getFamily() == Response.Status.Family.CLIENT_ERROR)
		{
			logger.info("REST " + requestContext.getMethod() + " Response : " + sb.toString());
		} else
		{
			logger.error("REST " + requestContext.getMethod() + " Response : " + sb.toString());
		}
	}
	
	/**
	 * Здесь мы добавляем к ответу заголовки
	 * <p>
	 * чтобы веб-модуль мог работать и на других серверах
	 * <p>
	 * а не только на том сервере на котором запущено API
	 * <p>
	 * @param responseContext контекст ответа на запрос
	 */
	private void addHeader(ContainerResponseContext responseContext)
	{
		responseContext.getHeaders().add("Access-Control-Allow-Origin", "*");
		responseContext.getHeaders().add("Access-Control-Allow-Headers",
				"Origin, X-Requested-With, Content-Type, Accept, Key, Authorization");
		responseContext.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
	}
}