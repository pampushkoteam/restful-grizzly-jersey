package pampushko.tests.grizzlyRest.constants;

/**
 *
 */
public interface RegexpPatterns
{
	String UUID_PATTERN = "^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$";
}
