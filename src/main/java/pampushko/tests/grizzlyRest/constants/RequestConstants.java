package pampushko.tests.grizzlyRest.constants;

/**
 * Класс для хранение констант
 */
public interface RequestConstants
{
	String REQUEST_CONTEXT = "context";
	String REQUEST_ID = "request_id";
}
