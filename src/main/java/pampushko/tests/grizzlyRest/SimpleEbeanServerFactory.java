package pampushko.tests.grizzlyRest;

import io.ebean.EbeanServer;
import io.ebean.EbeanServerFactory;
import io.ebean.config.ServerConfig;
import io.ebean.config.UnderscoreNamingConvention;
import org.avaje.datasource.DataSourceConfig;
import org.glassfish.hk2.api.Factory;

/**
 *
 */
public class SimpleEbeanServerFactory implements Factory<EbeanServer>

{
	//todo как добавить переменные среды?
	//@Inject
	//private Environment environment;
	
	@Override
	public EbeanServer provide()
	{
		final ServerConfig serverConfig = new ServerConfig();
		DataSourceConfig dataSourceConfig = new DataSourceConfig();
		serverConfig.setDataSourceConfig(dataSourceConfig);
		
		//можно загрузить конфиг из файла ebean.properties
		//или можно указать файл с другим названием,
		//но тогда нужно другой файл передать в метод в качестве параметра
		serverConfig.loadFromProperties();
		
		serverConfig.setDdlGenerate(true);
		serverConfig.setDdlRun(true);
		
		serverConfig.setDefaultServer(true);
		serverConfig.setRegister(true); //todo что это значит?
		serverConfig.setNamingConvention(new UnderscoreNamingConvention());
		
		EbeanServer eServer = null;
		//делаем попытку создать сервер по конфигу, если нельзя создать (таблицы уже есть, то отключаем
		// необходимость создавать таблицы и выполняем еще одну попытку создать сервер ebean)
		try
		{
			eServer = EbeanServerFactory.create(serverConfig);
		}
		catch (Exception ex)
		{
			serverConfig.setDdlRun(false);
			eServer = EbeanServerFactory.create(serverConfig);
		}
		return eServer;
	}
	
	@Override
	public void dispose(EbeanServer instance)
	{
	
	}
}
