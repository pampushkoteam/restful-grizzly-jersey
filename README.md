## Описание проекта
 
Этот проект демонстрирует как делать REST сервисы с использованием таких технологий: 

* встроенный сервер: [Grizzly](https://grizzly.java.net/)
* REST фреймворк: [Jersey](https://jersey.java.net/) 
* систему сборки [Maven](https://maven.apache.org/)

Делаем сервис закладок (Bookmarking service)

## Документация

В процессе разработки

## Лицензия

BSD

## Руководитель проекта

* Alexander Pampushko ([pampushko@gmail.com](mailto:pampushko@gmail.com))

## Участники

* Alexander Pampushko
